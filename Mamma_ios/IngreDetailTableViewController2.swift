//
//  IngreDetailTableViewController.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2017. 1. 3..
//  Copyright © 2017년 김민수. All rights reserved.
//


import UIKit
import Alamofire
import Kingfisher

class IngreDetailTableViewController2: UITableViewController, NetworkCallback{
    var reviewList = [IngreDetailVO]()
    var milk_image: String?
    var milk_name: String?
    var stage: Int?
    var milk_grade:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let model = IngredientsRankModel2(self)
        
        model.getReviewDetail(milk_image: gsno(milk_image), milk_name: gsno(milk_name), stage: gino(stage), milk_grade: gino(milk_grade))
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func networkResult(resultData: Any, code: Int) {
        reviewList = resultData as![IngreDetailVO]
        tableView.reloadData()
    }
}

extension IngreDetailTableViewController2{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviewList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "IngreCell", for: indexPath) as! IngreDetailTableViewCell
        
        let item = reviewList[indexPath.row]
        
        if let url = URL(string: item.milk_image){
            cell.milk_imageView.kf.setImage(with: url)
            cell.milk_imageView.contentMode = .scaleAspectFit
        }
        cell.txtNumber.text = "\(indexPath.row + 1)"
        cell.milk_nametxt.text = gsno(item.milk_name)
        cell.milk_nametxt.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.milk_nametxt.numberOfLines = 2
        //        cell.imgType.image = item.imgType
        
        return cell
    }
}

