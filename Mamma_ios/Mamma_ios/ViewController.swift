//
//  ViewController.swift
//  Mamma_ios
//
//  Created by 김민수 on 2016. 12. 26..
//  Copyright © 2016년 김민수. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ViewController: UIViewController{
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var pwTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    let baseURL = "http://52.78.46.225:3000"
    
    
    @IBOutlet var idStore: UIButton!
    @IBOutlet var autoLogin: UIButton!
    
    internal var trueImg : UIImage?
    internal var falseImg : UIImage?
    
    var _idStore = 0
    var _autoLogin = 0
    var _idStoreString : String = ""
    
    
    @IBAction func idStore_Click(_ sender: Any) {
        if (_idStore == 0){
            
            
            print("idStore Click")
            
            
            
            _idStore = 1
            _idStoreString = idTextField.text!
            idStore.setImage(UIImage(named: "login_check.png"), for: UIControlState.normal)
            
            UserDefaults.standard.set(_idStore, forKey: "idStore")
            UserDefaults.standard.set(_idStoreString, forKey: "idStoreString")
            
            
            print("======  When Click id Store String value  =====")
            print(_idStoreString)
            
            
            let idStoreString = UserDefaults.standard.integer(forKey: "idStoreString")
            print("======  When Click id Store String value 2  =====")
            print(idStoreString)
            
        }
        else {
            print("idStore Click No ")
            
            
            _idStore = 0
            _idStoreString = idTextField.text!
            idStore.setImage(UIImage(named: "login_no_check.png"), for: UIControlState.normal)
            UserDefaults.standard.set(_idStore, forKey: "idStore")
            UserDefaults.standard.set(_idStoreString, forKey: "idStoreString")
        }
  }
    
    
    @IBAction func autoLogin_Click(_ sender: Any) {
        if (_autoLogin == 0){
            
            print("auto Click")
            
            _autoLogin = 1
            autoLogin.setImage(UIImage(named: "login_check.png"), for: UIControlState.normal)
            UserDefaults.standard.set(_autoLogin, forKey: "autoLogin")
        }
        else {
            
            print("auto Click No  ")
            
            _autoLogin = 0
            autoLogin.setImage(UIImage(named: "login_no_check.png"), for: UIControlState.normal)
            UserDefaults.standard.set(_autoLogin, forKey: "autoLogin")
        }
    }
    
    
    @IBAction func loginAction(_ sender: Any) {
        if (idTextField.text?.isEmpty)! {
            simpleAlert(title: "입력 오류", msg: "보호자의 이메일을 입력해주세요.")
        } else if (pwTextField.text?.isEmpty)!{
            simpleAlert(title: "입력 오류", msg: "보호자의 비밀번호를 입력해주세요.")
        } else {
            let params = [
                "p_email" : idTextField.text!,
                "p_password" : pwTextField.text!
            ]
            Alamofire.request("\(baseURL)/sign/in", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { res in
                switch res.result {
                case .success :
                    if let value = res.result.value {
                        let data = JSON(value)
                        let result = data["result"]["signresult"]["signin"].int
                            if( result == 1){
                            let nickName = data["result"]["signresult"]["p_nickname"].string
                            UserDefaults.standard.set(result!, forKey: "LoginChk")
                            UserDefaults.standard.set(nickName!, forKey: "NickName")
                                
                                self._idStoreString = self.idTextField.text!
                            UserDefaults.standard.set(self._idStoreString, forKey: "idStoreString")
                                
                                
                            let loginChk = UserDefaults.standard.integer(forKey: "LoginChk")
                            if (loginChk == 1) {
                                UserDefaults.standard.set(self.idTextField.text!, forKey: "ID")
                                if let svc = self.storyboard?.instantiateViewController(withIdentifier: "Main")  {
                                    self.present(svc, animated: false)
                                }
                            }
                        }
                        else{
                            print("Login Fail")
//                            simpleAlert(title: "실패", msg: "로그인 실패")
                        }
                    }
                case .failure(let err) :
                    print(err)
                }
            }
        }
    }

    @IBAction func regButton(_ sender: Any) {
        print("regbutton")
                if let svc = storyboard?.instantiateViewController(withIdentifier: "regMom") as? regMomViewController {
                    print(" in ? ")
                    self.navigationController?.pushViewController(svc, animated: true)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        self.viewDidLoad()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("auto Login check")
        
        //아무 곳을 누르면 hide
        idTextField.delegate = self
        pwTextField.delegate = self
        
        
        //아무 곳을 누르면 hide
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(regMomViewController.DismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        
        
        
        
        let autoLogin = UserDefaults.standard.integer(forKey: "autoLogin")
        let idStore = UserDefaults.standard.integer(forKey: "idStore")
        let idStoreString = UserDefaults.standard.string(forKey: "idStoreString")!
        
        print("======  auto Login value  =====")
        print(autoLogin)
        print("======  id Store value  =====")
        print(idStore)
        print("======  id Store String value  =====")
        print(idStoreString)
        
        
        
        if( autoLogin == 1){
            if let svc = self.storyboard?.instantiateViewController(withIdentifier: "Main")  {
                self.present(svc, animated: false)
            }
        }
        else if( idStore == 1 ){
            idTextField.text = UserDefaults.standard.string(forKey: "idStoreString")
           self.idStore.setImage(UIImage(named: "login_check.png"), for: UIControlState.normal)
        }
    }
}





extension ViewController :  UITextFieldDelegate{
    
    //리턴 버튼을 누르면 hide
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when   'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        idTextField.resignFirstResponder()
        pwTextField.resignFirstResponder()
        
        self.view.endEditing(true)
    }
    
    //리턴 버튼을 누르면 hide
    func DismissKeyboard(){
        self.view.endEditing(true)
    }
    //    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    //        self.view.endEditing(true)
    //    }
    
}

