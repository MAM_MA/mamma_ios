//
//  FloatingViewController.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2016. 12. 30..
//  Copyright © 2016년 김민수. All rights reserved.
//

import UIKit
import KCFloatingActionButton

class FloatingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fab = KCFloatingActionButton()
        fab.addItem(title: "Hello, World!")
        self.view.addSubview(fab)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
