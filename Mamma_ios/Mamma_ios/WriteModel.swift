//
//  WriteModel.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2017. 1. 4..
//  Copyright © 2017년 김민수. All rights reserved.
//

import Alamofire
import SwiftyJSON

class WriteModel:NetworkModel {

    func reviewWrite(
         good: String,
         bad: String,
         tip: String,
         title:String,
         grade : Int
        ) {
        
        let params = [
            "good" : good,
            "bad" : bad,
            "tip" : tip,
            "title" : title,
            "grade" : grade,
            ] as [String : Any]

        /*
        let BASEURL = "\(baseURL)/review/post/"
        var tmpPara : String = "노발락 1단계"
        tmpPara = tmpPara + UserDefaults.standard.string(forKey: "NickName")!
        
        let encodeQuery = tmpPara.addingPercentEscapes(using: .utf8)
        let requestUrl = BASEURL + encodeQuery!
        
        Alamofire.request(requestUrl).responseJSON() { res in
        
        */

        Alamofire.request("\(baseURL)/review/post/tmpMilk/tmpWriter", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { res in
            switch res.result {
            case .success :
                self.view.networkResult(resultData: "", code: 0)
                break
            case .failure(let err) :
                break
            }
        }
    }
}
