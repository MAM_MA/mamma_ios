//
//  regBabyViewController.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2016. 12. 27..
//  Copyright © 2016년 김민수. All rights reserved.
//

import UIKit

class regBabyViewController: UIViewController, NetworkCallback {
    
    
    
    @IBOutlet weak var babyNameText: UITextField!
    @IBOutlet weak var babyBirthText: UITextField!
    
    @IBOutlet var sexGirl: UIButton!
    
    @IBOutlet var sexMan: UIButton!
    
    @IBOutlet var btnSeq1: UIButton!
    @IBOutlet var btnSeq2: UIButton!
    @IBOutlet var btnSeq3: UIButton!
    @IBOutlet var btnSeq4: UIButton!
    
    
    var _sexGirl : Int = 0
    var _sexMan : Int = 0
    
    var _btnSeq1 : Int = 0
    var _btnSeq2 : Int = 0
    var _btnSeq3 : Int = 0
    var _btnSeq4 : Int = 0
    
    var string_sexGirl = ""
    var string_sexMan = ""
    var string_sex = ""
    
    var string_btnSeq1 = ""
    var string_btnSeq2 = ""
    var string_btnSeq3 = ""
    var string_btnSeq4 = ""
    
    
    var rdata_email = ""
    var rdata_name = ""
    var rdata_pw = ""
    var rdata_pw_check = ""
    var rdata_birth = ""
    var rdata_nick = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "회원가입"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "확인", style: .plain, target: self, action: #selector(okTapped))
    }
    
    
    func okTapped (sender:UIButton) {
        let momEmailText = rdata_email
        let momNameText = rdata_name
        let momPwText = rdata_pw
        //        let momPwCheckText = rdata_pw_check
        let momBirthText = rdata_birth
        let momNickText = rdata_nick
        
        let babyNameText = self.babyNameText.text!
        let babyBirthText = self.babyBirthText.text!
        
        if babyNameText.isEmpty {
            simpleAlert(title: "입력 오류", msg: "이메일을 입력해주세요.")
        } else if babyBirthText.isEmpty {
            simpleAlert(title: "입력 오류", msg: "비밀번호를 입력해주세요.")
        }else {
            let model = UserModel(self)
            model.signUp(p_email: momEmailText,
                         p_name: momNameText,
                         p_password: momPwText,
                         p_birth: momBirthText,
                         p_nickname: momNickText,
                         b_name: babyNameText,
                         b_birth: babyBirthText,
                         b_sex: string_sex,
                         b_worry1 : string_btnSeq2,
                         b_worry2 : string_btnSeq3,
                         b_worry3 : string_btnSeq4
                        )
        }
    }
    
    func networkResult(resultData: Any, code: Int) {
        //        let mainSB = UIStoryboard(name: "Main", bundle: nil) // 로그인과 메인의 스토리보드가 달라서 이렇게 만들어준다.
        let vc = storyboard?.instantiateViewController(withIdentifier: "Login")
        present(vc!, animated: true)
    }
    
    
    
    @IBAction func sexGirl_Click(_ sender: Any) {
        if (_sexGirl == 0 ){
            _sexGirl = 1
            _sexMan = 0
            string_sex = "여"
            changeImg(_sexGirl)
        }
        else {
            _sexGirl = 0
            _sexMan = 1
            string_sex = "남"
            changeImg(_sexGirl)
        }
    }

   
    
    @IBAction func sexMan_Click(_ sender: Any) {
        if (_sexMan == 0 ){
            _sexGirl = 0
            _sexMan = 1
            string_sex = "남"
            changeImg(_sexGirl)
        }
        else {
            _sexGirl = 1
            _sexMan = 0
            string_sex = "여"
            changeImg(_sexGirl)
        }
    }
    
    func changeImg(_ value: Int){
        if (value == 1){
            sexGirl.setImage(UIImage(named: "Group 4128.png"), for: UIControlState.normal)
            sexMan.setImage(UIImage(named: "Group 4135.png"), for: UIControlState.normal)
        }
        else{
            sexGirl.setImage(UIImage(named: "Group 4134.png"), for: UIControlState.normal)
            sexMan.setImage(UIImage(named: "Group 4129.png"), for: UIControlState.normal)
        }
    }
    
    
    
    @IBAction func btnSeq1_Click(_ sender: Any) {
        if (_btnSeq1 == 1){
            _btnSeq1 = 0
            btnSeq1.setImage(UIImage(named: "Group 4130.png"), for: UIControlState.normal)
        }
        else{
            if(_btnSeq2 + _btnSeq3 + _btnSeq4 > 0){
             print("Not Accept ")
            }
            else{
            _btnSeq1 = 1
            btnSeq1.setImage(UIImage(named: "Group 4136.png"), for: UIControlState.normal)
            }
        }
    }
    
    
    
    
    
    
    
    @IBAction func btnSeq2_Click(_ sender: Any) {
        if (_btnSeq1 == 0){
            if (_btnSeq2 == 1){
            _btnSeq2 = 0
                string_btnSeq2 = ""
            btnSeq2.setImage(UIImage(named: "Group 4131.png"), for: UIControlState.normal)
            }
            else{
                _btnSeq2 = 1
                string_btnSeq2 = "알레르기"
                btnSeq2.setImage(UIImage(named: "Group 4137.png"), for: UIControlState.normal)
            }
        }
        else{
        }
    }
    
    
    
    
    @IBAction func btnSeq3_Click(_ sender: Any) {
        if (_btnSeq1 == 0){
        if (_btnSeq3 == 1){
            _btnSeq3 = 0
            string_btnSeq3 = ""
            btnSeq3.setImage(UIImage(named: "Group 4138.png"), for: UIControlState.normal)
        }
        else{
            _btnSeq3 = 1
            string_btnSeq3 = "아토피"
            btnSeq3.setImage(UIImage(named: "Group 4144.png"), for: UIControlState.normal)
            }
        }
        else{
        }
    }
    
    
    
    @IBAction func btnSeq4_Click(_ sender: Any) {
    if (_btnSeq1 == 0){
        if (_btnSeq4 == 1){
            _btnSeq4 = 0
            string_btnSeq4 = ""
            btnSeq4.setImage(UIImage(named: "Group 4133.png"), for: UIControlState.normal)
        }
        else{
            _btnSeq4 = 1
            string_btnSeq4 = "배변"
            btnSeq4.setImage(UIImage(named: "Group 4139.png"), for: UIControlState.normal)
        }
        }
        else{
        }
    }

    
    
    
    
    
}
