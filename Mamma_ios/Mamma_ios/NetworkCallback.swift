//
//  NetworkCallback.swift
//  Mamma_ios
//
//  Created by 김민수 on 2017. 1. 2..
//  Copyright © 2017년 김민수. All rights reserved.
//

protocol NetworkCallback {
    func networkFailed()
    func networkResult(resultData: Any, code: Int)
}
