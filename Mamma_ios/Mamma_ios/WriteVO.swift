//
//  WriteVO.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2017. 1. 4..
//  Copyright © 2017년 김민수. All rights reserved.
//

import UIKit
import Kingfisher

class WriteVO {
    var good: String
    var bad: String
    var tip: String
    var title : String
    var grade : Int
    
    init(good:String, bad:String, tip:String, title:String){
        self.good = good
        self.bad = bad
        self.tip = tip
        self.title = title
        self.grade = 0   // 별점 그런데 ios에서는 해당사항 없음
        // 안드로이드때문에 api에 있으니 ios에서는 그냥 아무 값이나 전송
    }
}
