//
//  ReviewViewController.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2016. 12. 29..
//  Copyright © 2016년 김민수. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import KCFloatingActionButton

class ReviewViewController: UIViewController {
    
    @IBOutlet weak var tx: CustomTextField!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var reviewImg1: UIImageView!
    @IBOutlet var reviewImg2: UIImageView!
    @IBOutlet var reviewImg3: UIImageView!
    
    @IBOutlet var reviewCmpy1: UILabel!
    @IBOutlet var reviewCmpy2: UILabel!
    @IBOutlet var reviewCmpy3: UILabel!
    
    @IBOutlet var reviewName1: UILabel!
    @IBOutlet var reviewName2: UILabel!
    @IBOutlet var reviewName3: UILabel!
    
    
    @IBOutlet weak var myNickName: UILabel!
    
    @IBOutlet var reviewBottomProductImg: UIImageView!
    @IBOutlet var reviewBottomProductName: UILabel!
    
    @IBOutlet var reviewBottomNickName: UILabel!
    
    @IBOutlet var reviewBottomAge: UILabel!
    

    @IBOutlet weak var reviewBottomAttribute1: UIImageView!
    
    
    @IBOutlet weak var reviewBottomAttribute2: UIImageView!
    
    
    @IBOutlet weak var reviewBottomAttribute3: UIImageView!
    
    
    
    
    @IBOutlet weak var btnReviewWrite: UIButton!
    
    

    @IBOutlet var reviewBottomOneLine: UILabel!
    
    
    
    @IBAction func MoreReview_Click(_ sender: Any) {
        print("reviewDetail")
        if let svc = storyboard?.instantiateViewController(withIdentifier: "reviewDetail") as? ReviewDetailTableViewController {
            navigationController?.pushViewController(svc, animated: true)
        }
        
//        if let svc = self.storyboard?.instantiateViewController(withIdentifier: "reviewDetail") as? ReviewDetailTableViewController {
//            self.present(svc, animated: false)
//        }

        
    }
    
    
    @IBAction func btnReviewWrite_Click(_ sender: Any) {
        if let svc = self.storyboard?.instantiateViewController(withIdentifier: "reviewWrite") as? ReviewWriteViewController {
            self.navigationController?.pushViewController(svc, animated: true)
            }
    }
    
    
    
    
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentSize.height = 1000
        navigationController?.navigationBar.isHidden = true
        //아무 곳을 누르면 hide
        tx.delegate = self
        
        self.myNickName.text = UserDefaults.standard.string(forKey: "NickName")
        
        
        
        //아무 곳을 누르면 hide
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(regMomViewController.DismissKeyboard))
        self.view.addGestureRecognizer(tap)

       
        reviewCmpy1.numberOfLines = 3;
        reviewName1.numberOfLines = 3;
        
        reviewCmpy2.numberOfLines = 3;
        reviewName2.numberOfLines = 3;
        
        reviewCmpy3.numberOfLines = 3;
        reviewName3.numberOfLines = 3;
        
        reviewBottomProductName.numberOfLines = 3;
        reviewBottomOneLine.numberOfLines = 3;

                scrollView.contentSize.height = 1000
        navigationController?.navigationBar.isHidden = true
        let baseURL = "http://52.78.46.225:3000"
        let BASEURL = "\(baseURL)/reviewview/"
        var query : String = ""
        query = UserDefaults.standard.string(forKey: "NickName")!
        let encodeQuery = query.addingPercentEscapes(using: .utf8)
        let requestUrl = BASEURL + encodeQuery!
        
        Alamofire.request(requestUrl).responseJSON() { res in
        
//        Alamofire.request("\(baseURL)/reviewview/333", method: .get, encoding:
//            JSONEncoding.default).responseJSON{ res in
                switch res.result {
                case .success :
                    print("join")
                    if let value = res.result.value {
                        let data = JSON(value)
                        var array = data["result"]["reviewrank"].array
                        
                        self.reviewCmpy1.text = array?[0]["milk_company"].string
                        self.reviewName1.text = array?[0]["milk_name"].string
                        if let strUrl = array?[0]["milk_image"].string {
                            if let url = URL(string: strUrl) {
                                self.reviewImg1.kf.setImage(with: url)
                            }
                        }
                        
                        self.reviewCmpy2.text = array?[1]["milk_company"].string
                        self.reviewName2.text = array?[1]["milk_name"].string
                        if let strUrl = array?[1]["milk_image"].string {
                            if let url = URL(string: strUrl) {
                                self.reviewImg2.kf.setImage(with: url)
                            }
                        }
                        
                        self.reviewCmpy3.text = array?[2]["milk_company"].string
                        self.reviewName3.text = array?[2]["milk_name"].string
                        if let strUrl = array?[2]["milk_image"].string {
                            if let url = URL(string: strUrl) {
                                self.reviewImg3.kf.setImage(with: url)
                            }
                        }
                        
                        self.reviewBottomNickName.text = data["result"]["similar"]["nickname"].string
                        self.reviewBottomProductName.text = data["result"]["similar"]["milk_name"].string
                        self.reviewBottomAge.text = String(describing: data["result"]["similar"]["age"]) + "개월 수"
                        self.reviewBottomOneLine.text = data["result"]["similar"]["title"].string

                        
                        if(data["result"]["similar"]["worry"][0].string != ""){
                            self.reviewBottomAttribute1.image = UIImage(named: "Group 4137")
                        }
                        else{
                            self.reviewBottomAttribute1.image = UIImage(named: "Group 4131")
                        }
                        
                        if(data["result"]["similar"]["worry"][1].string != ""){
                            self.reviewBottomAttribute2.image = UIImage(named: "Group 4139")
                        }
                        else{
                            self.reviewBottomAttribute2.image = UIImage(named: "Group 4133")
                        }
                        
                        if(data["result"]["similar"]["worry"][2].string != ""){
                            self.reviewBottomAttribute3.image = UIImage(named: "Group 4144")
                        }
                        else{
                            self.reviewBottomAttribute3.image = UIImage(named: "Group 4138")
                        }
                        
                        if let strUrl = data["result"]["similar"]["milk_image"].string {
                            if let url = URL(string: strUrl) {
                                self.reviewBottomProductImg.kf.setImage(with: url)
                            }
                        }
                    }
                    break
                case .failure(let err) :
                    print(err)
                }
        }
        
    
        /*
        let fab = KCFloatingActionButton()
        
        fab.buttonImage = UIImage(named: "floating.png")
        fab.paddingY = 70
        fab.sticky = true
        fab.addItem("", icon: UIImage(named: "floating.png")!, handler: { _ in  print("floating")
            if let svc = self.storyboard?.instantiateViewController(withIdentifier: "reviewWrite") as? ReviewWriteViewController {
                self.navigationController?.pushViewController(svc, animated: true)
            }
        }
        )
        self.view.addSubview(fab)
 */
 }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



extension ReviewViewController :  UITextFieldDelegate{
    
    //리턴 버튼을 누르면 hide
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when   'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        tx.resignFirstResponder()
        
        
        self.view.endEditing(true)
    }
    
    //리턴 버튼을 누르면 hide
    func DismissKeyboard(){
        self.view.endEditing(true)
    }
    //    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    //        self.view.endEditing(true)
    //    }
    
}

