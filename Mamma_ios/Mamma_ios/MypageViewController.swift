//
//  MypageViewController.swift
//  Mamma_ios
//
//  Created by 김민수 on 2016. 12. 29..
//  Copyright © 2016년 김민수. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MypageViewController: UIViewController
{
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var myPageMomName: UILabel!
    @IBOutlet var myPageBabyName: UILabel!
    @IBOutlet var myPageNickName: UILabel!
    @IBOutlet var myPageBabyAge: UILabel!
    
    @IBOutlet var myPageAttribute1: UILabel!
    @IBOutlet var myPageAttribute2: UILabel!
    @IBOutlet var myPageAttribute3: UILabel!
    @IBOutlet var myPageAttribute4: UILabel!

    let baseURL = "http://52.78.46.225:3000"
    
    
    @IBAction func editMyPage_Click(_ sender: Any) {
        print("editMyPage")
        if let svc = storyboard?.instantiateViewController(withIdentifier: "editMyPage") as? EditMyPageViewController {
            navigationController?.pushViewController(svc, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        scrollView.contentSize.height = 1000
//        
//        Alamofire.request("\(baseURL)/mypage/tmpWriter", method: .get, encoding: JSONEncoding.default).responseJSON { res in
//            switch res.result {
//            case .success :
//                if let value = res.result.value {
//                    let data = JSON(value)
//                    
//                    self.myPageMomName.text = data["result"]["parent"]["p_name"].string
//                    self.myPageNickName.text = data["result"]["parent"]["p_nickname"].string
//                    self.myPageBabyName.text = data["result"]["baby"]["b_name"].string
//                    
//                    self.myPageAttribute1.text = data["result"]["baby"]["worry1"].string
//                    self.myPageAttribute2.text = data["result"]["baby"]["worry2"].string
//                    self.myPageAttribute3.text = data["result"]["baby"]["worry3"].string
//                    
//                    
//                    /*
//                    self.myPageBabyName.text
//                    self.myPageNickName.text
//                    self.myPageBabyAge.text
//                    
//                    self.myPageAttribute1.text
//                    self.myPageAttribute2.text
//                    self.myPageAttribute3.text
//                    self.myPageAttribute4.text
//                    */
//                    /*
//                    self.reviewBottomNickName.text = data["result"]["similar"]["nickname"].string
//                    
//                    let data = JSON(value)
//                    var tempList = [ReviewDetailVO]()
//                    if let array = data["result"]["reviewRank"].array{
//                        for item in array{
//                            
//                            let uvo = ReviewDetailVO(milk_image: self.gsno(item["milk_image"].string),
//                                                     milk_name: self.gsno(item["milk_name"].string),
//                                                     stage: self.gino(item["stage"].int),
//                                                     milk_grade: self.gino(item["milk_grade"].int)
//                     
//                    self.reviewCmpy1.text = array?[0]["milk_company"].string
//                    self.reviewName1.text = array?[0]["milk_name"].string
//                    if let strUrl = array?[0]["milk_image"].string {
//                        if let url = URL(string: strUrl) {
//                            self.reviewImg1.kf.setImage(with: url)
//                        }
//                    }
//                    */
//                }
//            case .failure(let err) :
//                print(err)
//                print("Login Fail")
//            }
//        }
    
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "로그아웃", style: .plain, target: self, action: #selector(logoutTapped))
        }
    
    func logoutTapped (sender:UIButton) {
        UserDefaults.standard.set(0, forKey: "autoLogin")
        
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "init")
        present(vc!, animated: true)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
