//
//  TabySegmentedControl.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2017. 1. 3..
//  Copyright © 2017년 김민수. All rights reserved.
//

import UIKit

class TabySegmentedControl: UISegmentedControl {
    func initUI(){
//        setupBackground()
//        setupFonts()
    }
    
    func setupBackground(){
        let backgroundImage = UIImage(named: "stepBackGround")
//        let dividerImage = UIImage(named: "uses")
//        let backgroundImageSelected = UIImage(named: "smile")
        
        self.setBackgroundImage(backgroundImage, for: UIControlState(), barMetrics: .default)
//        self.setBackgroundImage(backgroundImageSelected, for: .highlighted, barMetrics: .default)
//        self.setBackgroundImage(backgroundImageSelected, for: .selected, barMetrics: .default)
        
        
        /*
        self.setDividerImage(dividerImage, forLeftSegmentState: UIControlState(), rightSegmentState: .selected, barMetrics: .default)
        self.setDividerImage(dividerImage, forLeftSegmentState: .selected, rightSegmentState: UIControlState(), barMetrics: .default)
        self.setDividerImage(dividerImage, forLeftSegmentState: UIControlState(), rightSegmentState: UIControlState(), barMetrics: .default)
 */
    }
    
    func setupFonts(){
        let font = UIFont.systemFont(ofSize: 16.0)
        
        
        let normalTextAttributes = [
            NSForegroundColorAttributeName: UIColor.black,
            NSFontAttributeName: font
        ]
        
        self.setTitleTextAttributes(normalTextAttributes, for: UIControlState())
        self.setTitleTextAttributes(normalTextAttributes, for: .highlighted)
        self.setTitleTextAttributes(normalTextAttributes, for: .selected)
    }
}
