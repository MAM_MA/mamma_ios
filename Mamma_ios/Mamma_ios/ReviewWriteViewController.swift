//
//  ReviewWrite.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2016. 12. 29..
//  Copyright © 2016년 김민수. All rights reserved.
//

import UIKit

class ReviewWriteViewController: UIViewController, NetworkCallback{
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet var reviewWriteTitle: UITextField!
    @IBOutlet var reviewWriteGood: UITextField!
    @IBOutlet var reviewWriteBad: UITextField!
    @IBOutlet var reviewWriteTip: UITextField!
    
    @IBOutlet weak var reWriteOption1: UIImageView!
    @IBOutlet weak var reWriteOption2: UIImageView!
    @IBOutlet weak var reWriteOption3: UIImageView!
    @IBOutlet weak var reWriteOption4: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        reviewWriteTitle.delegate = self
        reviewWriteGood.delegate = self
        reviewWriteBad.delegate = self
        reviewWriteTip.delegate = self

        
        
        //아무 곳을 누르면 hide
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(regMomViewController.DismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        
        
        
        scrollView.contentSize.height = 1000
        navigationController?.navigationBar.isHidden = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "확인", style: .plain, target: self, action: #selector(okTapped))
    }
    
    func okTapped (sender:UIButton) {
        let reviewWriteTitle = self.reviewWriteTitle.text!
        let reviewWriteGood = self.reviewWriteGood.text!
        let reviewWriteBad = self.reviewWriteBad.text!
        let reviewWriteTip = self.reviewWriteTip.text!
        
        
        if reviewWriteTitle.isEmpty || reviewWriteGood.isEmpty || reviewWriteBad.isEmpty || reviewWriteTitle.isEmpty {
            simpleAlert(title: "입력 오류", msg: "빈칸없이 입력해주세요.")
        }
        else{
            let model = WriteModel(self)
            model.reviewWrite(
                good : reviewWriteGood,
                bad : reviewWriteBad,
                tip : reviewWriteTip,
                title : reviewWriteTitle,
                grade : 0)
        }
    }
    
    func networkResult(resultData: Any, code: Int) {
        print("Review Write OK or Fail ")
    }
}


extension ReviewWriteViewController :  UITextFieldDelegate{
    
    //리턴 버튼을 누르면 hide
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when   'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
  
        
        reviewWriteTitle.resignFirstResponder()
        reviewWriteGood.resignFirstResponder()
        reviewWriteBad.resignFirstResponder()
        reviewWriteTip.resignFirstResponder()
        
        self.view.endEditing(true)
    }
    
    //리턴 버튼을 누르면 hide
    func DismissKeyboard(){
        self.view.endEditing(true)
    }
    //    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    //        self.view.endEditing(true)
    //    }
    
}

