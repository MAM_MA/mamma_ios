//
//  UserVO.swift
//  Mamma_ios
//
//  Created by 김민수 on 2017. 1. 3..
//  Copyright © 2017년 김민수. All rights reserved.
//

import UIKit

class UserVO {
    
    //가입변수 엄마
    var p_email: String
    var p_name: String
    var p_password: String
    var p_birth: String
    var p_nickname: String
//    var p_image: String?
    
    //가입변수 자식
    var b_name:String
    var b_birth:String
    
    //초기화
    init(p_email: String,
         p_name:String,
         p_password: String,
         p_birth: String,
         p_nickname: String,
//         p_image:String,
         b_name:String,
         b_birth:String) {
        
        self.p_email = p_email
        self.p_name = p_name
        self.p_password = p_password
        self.p_birth = p_birth
        self.p_nickname = p_nickname
//        self.p_image? = p_image
        
        self.b_name = b_name
        self.b_birth = b_birth
        
        
    }
    
    
    
    
    //    var imgType: UIImage {
    //        switch pokemon {
    //        case 100:
    //            return #imageLiteral(resourceName: "leaf")
    //        case 200:
    //            return #imageLiteral(resourceName: "fire")
    //        case 300:
    //            return #imageLiteral(resourceName: "water")
    //        default:
    //            return #imageLiteral(resourceName: "leaf")
    //        }
    //    }
}
