//
//  SeconedViewController.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2016. 12. 27..
//  Copyright © 2016년 김민수. All rights reserved.
//

import UIKit

class regMomViewController: UIViewController {

    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var pwText: UITextField!
    @IBOutlet weak var pw_check_Text: UITextField!
    @IBOutlet weak var birthText: UITextField!
    @IBOutlet weak var nickText: UITextField!
    
    
    
    @IBOutlet var btnAllAgree: CheckBox!
    @IBOutlet var btnUseTerms: CheckBox!
    @IBOutlet var btnPrivateInfo: CheckBox!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "회원가입"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "다음", style: .plain, target: self, action: #selector(nextTapped))
        
        //아무 곳을 누르면 hide
        emailText.delegate = self
        nameText.delegate = self
        pwText.delegate = self
        pw_check_Text.delegate = self
        birthText.delegate = self
        nickText.delegate = self
        
        
        
        //아무 곳을 누르면 hide
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(regMomViewController.DismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    func nextTapped (sender:UIButton) {
        if(btnUseTerms.checked && btnPrivateInfo.checked ){
            print("2개다 체크 완료")
            
            let tmp = String(describing: pwText.text)
            let tmp2 = String(describing: pw_check_Text.text)
            
            if(tmp == tmp2 ){
        let emailData = emailText.text
        let nameData = nameText.text
        let pwData = pwText.text
        let pw_checkData = pw_check_Text.text
        let birthData = birthText.text
        let nickData = nickText.text
        
                
                
        if let svc = storyboard?.instantiateViewController(withIdentifier: "regBaby") as? regBabyViewController {
            svc.rdata_email = emailData!
            svc.rdata_name = nameData!
            svc.rdata_pw = pwData!
            svc.rdata_pw_check = pw_checkData!
            svc.rdata_birth = birthData!
            svc.rdata_nick = nickData!
            navigationController?.pushViewController(svc, animated: true)
            }
            }
            else{
           simpleAlert(title: "에러", msg: "비밀번호 불일치 !")
            }
        }
        else{
            print("약관 체크 해주세요 !")
           simpleAlert(title: "동의 미설정", msg: "약관을 모두 체크해주세요 !")
        }
    }
    override func simpleAlert(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "확인", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true)
    }

    @IBAction func btnUseTerms_Click(_ sender: Any) {
        if btnUseTerms.checked{
            if btnPrivateInfo.checked{
                btnAllAgree.setButtonChecked(true)
            }
        }
        else{
            btnAllAgree.setButtonChecked(false)
        }
    }
    
    @IBAction func btnPrivateInfo_Click(_ sender: Any) {
        if btnPrivateInfo.checked{
            if btnUseTerms.checked{
                btnAllAgree.setButtonChecked(true)
            }
        }
        else{
            btnAllAgree.setButtonChecked(false)
        }
    }
    
    @IBAction func btnAllAgree_Click(_ sender: Any) {
        if btnAllAgree.checked{
            btnUseTerms.setButtonChecked(true)
            btnPrivateInfo.setButtonChecked(true)
        }
        else{
            btnUseTerms.setButtonChecked(false)
            btnPrivateInfo.setButtonChecked(false)
        }
    }
    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}




extension regMomViewController :  UITextFieldDelegate{
    
    //리턴 버튼을 누르면 hide
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when   'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
      func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        emailText.resignFirstResponder()
        nameText.resignFirstResponder()
        pwText.resignFirstResponder()
        pw_check_Text.resignFirstResponder()
        birthText.resignFirstResponder()
        nickText.resignFirstResponder()
        self.view.endEditing(true)
    }
  
      //리턴 버튼을 누르면 hide
    func DismissKeyboard(){
        self.view.endEditing(true)
    }
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        self.view.endEditing(true)
//    }
    
}
