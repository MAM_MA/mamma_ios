//
//  CompViewController.swift
//  Mamma_ios
//
//  Created by 김민수 on 2016. 12. 29..
//  Copyright © 2016년 김민수. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

class IngredientViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
   
    let baseURL = "http://52.78.46.225:3000"


    @IBOutlet weak var spinnerTextField: CustomTextField!
   
    @IBOutlet weak var spinnerContent: UILabel!
    @IBOutlet weak var userText: UILabel!
    @IBOutlet weak var sideText: UILabel!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
/**********************************************/
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var company1: UILabel!
    @IBOutlet weak var name1: UILabel!
    
    
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var company2: UILabel!
    @IBOutlet weak var name2: UILabel!
    
    
    @IBOutlet weak var imageView3: UIImageView!
    @IBOutlet weak var company3: UILabel!
    @IBOutlet weak var name3: UILabel!
    
    
    
    @IBOutlet weak var imageView4: UIImageView!
    @IBOutlet weak var company4: UILabel!
    @IBOutlet weak var name4: UILabel!
    
    @IBOutlet weak var tx: UITextField!
    /**********************************************/
    
    
    
    /**********************************************/
   
    let pickerView = UIPickerView()
    
    var bgList = ["2-LG",
    "2-PG",
    "AMP",
    "CLA",
    "CMP",
    "DHA",
    "EGF",
    "EPA",
    "GMP",
    "IGF",
    "IMP",
    "L-시스틴",
    "L-아르기닌",
    "L-카르니틴",
    "L-트리토판",
    "lgG",
    "lgY",
    "MCT",
    "slgA",
    "TGF-베타",
    "TPA-CPP",
    "UMP",
    "갈락토실락토스",
    "갈락토올리고당",
    "감마-리놀렌산",
    "강글리오사이드",
    "구리",
    "글리코메크로펩타이드(GMP)",
    "나이아신",
    "나트륨",
    "뉴클레오타이드",
    "단백질",
    "당류",
    "라피노스",
    "락츄로스",
    "락타데린",
    "락토페린",
    "레시틴",
    "루테인",
    "리놀레산",
    "리놀렌산",
    "리소짐",
    "마그네슘",
    "말토올리고당",
    "망간",
    "메티오닌",
    "뮤신",
    "베타카로틴",
    "베타카제인",
    "비오틴",
    "비타민A",
    "비타민B1",
    "비타민B12",
    "비타민B2",
    "비타민B6",
    "비타민C",
    "비타민D",
    "비타민D3",
    "비타민E",
    "비타민K",
    "비타민K1",
    "비타민K2",
    "사이알릴올리고당",
    "세라마이드",
    "셀레늄",
    "스핑고마이엘린",
    "시알산",
    "아라키돈산",
    "아연",
    "알파-리놀렌산",
    "알파락트알부민",
    "염소",
    "엽산",
    "오스테오폰틴(락토폰틴)",
    "올레아마이드",
    "요오드",
    "유당",
    "이노시톨",
    "이눌린",
    "이소말토올리고당",
    "인",
    "인지질",
    "지방",
    "철분",
    "카제인포스포펩타이드(CPP)",
    "칼륨",
    "칼슘",
    "콜레스테롤",
    "콜린",
    "타우린",
    "탄수화물",
    "트랜스지방",
    "판토텐산",
    "포스파티딜세린",
    "포스파티딜에탄올아민",
    "포스파티딜이노시톨",
    "포스파티딜콜린",
    "포화지방",
    "폴리아민",
    "폴리페놀",
    "프락토올리고당",]

    func initPickerView() {
        
        let barFrame = CGRect(x: 0, y: 0, width: view.frame.width, height: 40)
        let bar = UIToolbar(frame: barFrame)
        let btnDone = UIBarButtonItem(title: "완료", style: .done, target: self, action: #selector(selectBG))
        
     
        
        
        
        let btnSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        
        bar.setItems([btnSpace, btnDone], animated: true)
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        spinnerTextField.text = "비타민B1"
        spinnerTextField.inputView = pickerView
        spinnerTextField.inputAccessoryView = bar
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bgList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return bgList[row]
    }
    
    func selectBG() {
        let row = pickerView.selectedRow(inComponent: 0)
        //텍스트필드에 글자 추가
        
    
        spinnerTextField.text = bgList[row]
        getIngreFromServer1()
        getIngreFromServer2()
        view.endEditing(true)
        
        
    }
    
    
 
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initPickerView()
        
        scrollView.contentSize.height = 700
        navigationController?.navigationBar.isHidden = true
        
        getIngreFromServer3()
        getIngreFromServer4()
        
        spinnerContent.numberOfLines = 0
        userText.numberOfLines = 0
        sideText.numberOfLines = 0
        company1.numberOfLines = 0
        name1.numberOfLines = 0
        company2.numberOfLines = 0
        name2.numberOfLines = 0
        company3.numberOfLines = 0
        name3.numberOfLines = 0
        company4.numberOfLines = 0
        name4.numberOfLines = 0
        

        //아무 곳을 누르면 hide
        tx.delegate = self
        
        
        
        //아무 곳을 누르면 hide
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(regMomViewController.DismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        
        
        
        
        
        
        let tap0 = UITapGestureRecognizer(target: self, action: #selector(IngredientViewController.tappedMe))
        imageView1.addGestureRecognizer(tap0)
        imageView1.isUserInteractionEnabled = true
        
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(IngredientViewController.tappedMe))
        imageView2.addGestureRecognizer(tap2)
        imageView2.isUserInteractionEnabled = true
        
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(IngredientViewController.tappedMe))
        imageView3.addGestureRecognizer(tap3)
        imageView3.isUserInteractionEnabled = true
        
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(IngredientViewController.tappedMe))
        imageView4.addGestureRecognizer(tap4)
        imageView4.isUserInteractionEnabled = true
        
    }
    func tappedMe()
    {
       goToSegmetation()
    }
    
    
    func goToSegmetation(){
        if let svc = self.storyboard?.instantiateViewController(withIdentifier: "parentIngreRank") as? ParentViewController {
            self.navigationController?.pushViewController(svc, animated: true)
        }
    }
}




extension IngredientViewController :  UITextFieldDelegate{
    
    //리턴 버튼을 누르면 hide
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when   'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        tx.resignFirstResponder()
        
        
        self.view.endEditing(true)
    }
    
    //리턴 버튼을 누르면 hide
    func DismissKeyboard(){
        self.view.endEditing(true)
    }
    //    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    //        self.view.endEditing(true)
    //    }
    
}



extension IngredientViewController{
    
    
    
    
    func getIngreFromServer4() {
        
        let BASEURL = "\(baseURL)/ingredientview/"
        let query = "비타민B1"
        let encodeQuery = query.addingPercentEscapes(using: .utf8)
        let requestUrl = BASEURL + encodeQuery!
   
        
        
        Alamofire.request(requestUrl).responseJSON() { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
                    
                    
                    
                    let ingreName = data["result"]["ingredient_info"]["ingre_name"].string
                    UserDefaults.standard.set(ingreName, forKey: "stepProduct")
                    
                    
                    
                    //                    if let ingre_name = data["result"]["ingredient_info"]["ingre_name"].string {
                    //                        self.bgList.append(self) = ingre_name
                    //                    }
                    
                    if let overview =  data["result"]["ingredient_info"]["overview"].string {
                        self.spinnerContent.text = overview
                    }
                    
                    if let uses =  data["result"]["ingredient_info"]["uses"].string {
                        self.userText.text = uses
                    }
                    
                    if let side =  data["result"]["ingredient_info"]["side"].string {
                        self.sideText.text = side
                    }
                    
                    
                }
                
                break
            case .failure(let err) :
                print(err)
            }
        }
    }
    

    
    
    

    func views3(index: Int) -> (UIImageView, UILabel, UILabel) {
        
        switch index {
        case 1:
            return (imageView2, name2, company2)
        case 2:
            return (imageView3, name3, company3)
        case 3:
            return (imageView4, name4, company4)
        default:
            return (imageView1, name1, company1)
        }
    }
    
    
    func getIngreFromServer3() {
        let BASEURL = "\(baseURL)/ingredientview/"
        let query = "비타민B1"
        let encodeQuery = query.addingPercentEscapes(using: .utf8)
        let requestUrl = BASEURL + encodeQuery!
        
        Alamofire.request(requestUrl).responseJSON() { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
                    let ingreName = data["result"]["ingredient_info"]["ingre_name"].string
                        UserDefaults.standard.set(ingreName, forKey: "stepProduct")
                   
                    if let array =  data["result"]["rank"].array{
                        for (i, item) in array.enumerated() {
                            let tuple = self.views2(index: i)
                            
                            if let strUrl = item["milk_image"].string {
                                
                                if let url = URL(string: strUrl) {
                                    tuple.0.kf.setImage(with: url)
                                }
                            }
                            
                            
                            if let milk_name = item["milk_name"].string {
                                tuple.1.text = milk_name
                            }
                            
                            if let milk_company = item["milk_company"].string {
                                tuple.2.text = milk_company
                            }
                        }
                    }
                }
                
                break
            case .failure(let err) :
                print(err)
            }
        }
    }
}



/**********************************************//**********************************************/
/**********************************************//**********************************************/


extension IngredientViewController{
    
    func getIngreFromServer1() {
        

        let BASEURL = "\(baseURL)/ingredientview/"
        let txtQuery = spinnerTextField.text!
        let query = txtQuery.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        let requestUrl = BASEURL + gsno(query)
        
//        let encodeQuery = spinnerTextField.text?.addingPercentEscapes(using: .utf8)
//        let item = gsno(encodeQuery)
//        let BASEURL = "\(baseURL)/ingredientview\(item)"
       
    
        Alamofire.request(requestUrl).responseJSON() { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
                    
                    
//                    if let ingre_name = data["result"]["ingredient_info"]["ingre_name"].string {
//                        self.bgList.append(self) = ingre_name
//                    }
                    
                    if let overview =  data["result"]["ingredient_info"]["overview"].string {
                        self.spinnerContent.text = overview
                    }
                    
                    if let uses =  data["result"]["ingredient_info"]["uses"].string {
                        self.userText.text = uses
                    }
                    
                    if let side =  data["result"]["ingredient_info"]["side"].string {
                        self.sideText.text = side
                    }
                    
                    
                }
                
                break
            case .failure(let err) :
                print(err)
            }
        }
    }
    
    
}



 /**********************************************/ /**********************************************/

extension IngredientViewController {
    
    
    func views2(index: Int) -> (UIImageView, UILabel, UILabel) {
        
        switch index {
        case 1:
            return (imageView2, name2, company2)
        case 2:
            return (imageView3, name3, company3)
        case 3:
            return (imageView4, name4, company4)
        default:
            return (imageView1, name1, company1)
        }
    }
    func getIngreFromServer2() {
        
        let BASEURL = "\(baseURL)/ingredientview/"
        let txtQuery = spinnerTextField.text!
        let query = txtQuery.addingPercentEncoding(withAllowedCharacters: .alphanumerics)

        var cnt : Int = 1
        
        let requestUrl = BASEURL + gsno(query)
        
        Alamofire.request(requestUrl).responseJSON() { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
                    var ingreName = data["result"]["ingredient_info"]["ingre_name"].string
                    UserDefaults.standard.set(ingreName, forKey: "stepProduct")
                    if let array =  data["result"]["rank"].array{
                        for (i, item) in array.enumerated() {
                            let tuple = self.views2(index: i)
                            
                            if let strUrl = item["milk_image"].string {
                                
                                if let url = URL(string: strUrl) {
                                    tuple.0.kf.setImage(with: url)
                                }
                            }
                            
                            
                            if let milk_name = item["milk_name"].string {
                                tuple.1.text = milk_name
                            }
                            
                            if let milk_company = item["milk_company"].string {
                                tuple.2.text = milk_company
                            }
                            
                            
                        }
                    }
                    
                }
                
                break
            case .failure(let err) :
                print(err)
            }
        }
    }
    
}
