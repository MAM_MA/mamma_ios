//
//  UserModel.swift
//  Mamma_ios
//
//  Created by 김민수 on 2017. 1. 3..
//  Copyright © 2017년 김민수. All rights reserved.
//

import Alamofire
import SwiftyJSON

class UserModel:NetworkModel {
    
    // 로그인
    func login(p_email: String, p_password: String) {
        let params = [
            "p_email" : p_email,
            "p_password" : p_password
        ]
        
        Alamofire.request("\(baseURL)/sign/in", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
//                    print(data)
                    let result = self.gino(data["result"].int)
                    
                    UserDefaults.standard.set(data["result"].int, forKey: "LoginChk")
                    
                    let p_email = self.gsno(data["p_email"].string) // 서버에서 만들어준 p_email를 뿌림
                    let tuple = (result, p_email)
                    self.view.networkResult(resultData: tuple, code: 0) // 매칭되는 경우 1, 매칭안되면 0값이 모내진다.
                }
            case .failure(let err) :
                print(err)
                self.view.networkFailed()
            }
        }
    }

    
    
    
// 회원가입
// 메소드는 각각의 요청을 의미한다.
    

    
    func signUp(p_email: String,
                p_name:String,
                p_password: String,
                p_birth: String,
                p_nickname: String,
                b_name:String,
                b_birth:String,
                b_sex:String,
                b_worry1:String,
                b_worry2:String,
                b_worry3:String
                ) {
        
        let params = [
            "p_email" : p_email,
            "p_name" : p_name,
            "p_password" : p_password,
            "p_birth" : p_birth,
            "p_nickname" : p_nickname,
            "b_name":b_name,
            "b_birth":b_birth,
            "sex":b_sex,
            "worry1":b_worry1,
            "worry2":b_worry2,
            "worry3":b_worry3
            ] as [String : Any]
        
        
        
        
        
        Alamofire.request("\(baseURL)/sign/up", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { res in
            switch res.result {
            case .success :
                self.view.networkResult(resultData: "", code: 0)
                break
            case .failure(let err) :
                print(err)
                self.view.networkFailed()
            }
        }
        
    }
/****************************************************/
    



}


