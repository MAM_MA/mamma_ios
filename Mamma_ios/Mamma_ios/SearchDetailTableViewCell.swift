//
//  SearchDetailTableViewCell.swift
//  Mamma_ios
//
//  Created by 김민수 on 2017. 1. 6..
//  Copyright © 2017년 김민수. All rights reserved.
//

import UIKit

class SearchDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var txtNumber: UILabel!
    
    @IBOutlet weak var milk_imageView: UIImageView!
    
    @IBOutlet weak var milk_nametxt: UILabel!
    
    @IBOutlet weak var imgType: UIImageView!
}
