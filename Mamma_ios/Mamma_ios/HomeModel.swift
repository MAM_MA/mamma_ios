//
//  HomeModel.swift
//  Mamma_ios
//
//  Created by 김민수 on 2017. 1. 3..
//  Copyright © 2017년 김민수. All rights reserved.
//

import Alamofire
import SwiftyJSON

class HomeModel: NetworkModel{
//    
//    func getReviewDetail(milk_image:String, milk_name:String, stage:Int, milk_grade:Int,url : String){
//        
//        Alamofire.request("\(baseURL)/\(url)").responseJSON { res in
//            switch res.result {
//            case .success :
//                if let value = res.result.value {
//                    let data = JSON(value)
//                    var tempList = [ReviewDetailVO]()
//                    
//                    if let array = data["result"]["reviewRank"].array{
//                        for item in array{
//                            
//                            let uvo = ReviewDetailVO(milk_image:self.gsno(item["milk_image"].string),
//                                                     milk_name: self.gsno(item["milk_name"].string),
//                                                     stage: self.gino(item["stage"].int),
//                                                     milk_grade: self.gino(item["milk_grade"].int)
//                            )
//                            tempList.append(uvo)
//                        }
//                        self.view.networkResult(resultData: tempList, code: 0)
//                    }
//                }
//                break
//            case .failure(let err) :
//                print(err)
//                self.view.networkFailed()
//            }
//        }
//    }
    

    func getHomeList(p_email: String) {
        
        Alamofire.request("\(baseURL)/homeview").responseJSON { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
                    var tempList = [HomeVO]()
                    if let array1 = data["result"]["searchrank"].array{
                        for item in array {
                            let uvo = UserVO(email: self.gsno(item["milk_image"].string),
                                             name: self.gsno(item["milk_company"].string),
                                             pokemon: self.gino(item["milk_name"].string)
                            )
                            tempList.append(uvo)
                        }
                    }
                    
                    self.view.networkResult(resultData: tempList, code: 0)
                    
                }
                
                break
            case .failure(let err) :
                print(err)
                self.view.networkFailed()
            }
        }
        
        
    }
}
