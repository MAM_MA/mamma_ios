//
//  ParentViewController.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2017. 1. 3..
//  Copyright © 2017년 김민수. All rights reserved.
//

import UIKit

class ParentViewController: UIViewController {
    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
        case thirdChildTab = 2
        case fourChildTab = 3
        case fiveChildTab = 4
    }
    
   
    
    
    //@IBOutlet var segment: TabySegmentedControl!
    
    @IBOutlet var segment: TabySegmentedControl!
    
    @IBOutlet weak var segmentedControl: TabySegmentedControl!
    @IBOutlet weak var contentView: UIView!
    
    var currentViewController: UIViewController?
    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "IngreCell") as? IngreDetailTableViewController
        return firstChildTabVC
    }()
    
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "IngreCell2") as? IngreDetailTableViewController2
        return secondChildTabVC
    }()
    
    lazy var thirdChildTabVC : UIViewController? = {
        let thirdChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "IngreCell3") as? IngreDetailTableViewController3
        return thirdChildTabVC
    }()
    lazy var fourChildTabVC : UIViewController? = {
        let fourChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "IngreCell4") as? IngreDetailTableViewController4
        return fourChildTabVC
    }()
    lazy var fiveChildTabVC : UIViewController? = {
        let fiveChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "IngreCell5") as? IngreDetailTableViewController5
        return fiveChildTabVC
    }()
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        segmentedControl.backgroundColor = UIColor.darkGray
        segmentedControl.initUI()
        segmentedControl.selectedSegmentIndex = TabIndex.firstChildTab.rawValue
        displayCurrentTab(TabIndex.firstChildTab.rawValue)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    
    // MARK: - Switching Tabs Functions
    /*
    원래 것
     */
    @IBAction func switchTabs(_ sender: UISegmentedControl) {
        
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParentViewController()
        
        displayCurrentTab(sender.selectedSegmentIndex)
 
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    
    /*
    func changeIndexImage2(_ index: Int) {
        for i in 0...4 {
            var _index : String = "_selected.png"
            
            
            print(" seledc ted ")
            
            if( index == i)
            {
                _index = String(i) + _index
                segmentedControl.setImage(UIImage(named:"test"), forSegmentAt: i)
//                segmentedControl.setImage(UIImage(named:"_index"), forSegmentAt: i)
            }
            else {
             //   print(" First ")
            //  print(_index)
                
                _index = "_no" + _index
            //    print(" Two ")
            //    print(_index)
                
                _index = String(i) + _index
             //   print(" Third ")
             //   print(_index)
                segmentedControl.setImage(UIImage(named:"_index"), forSegmentAt: i)
            }
        }
    }
    */
    func changeIndexImage(_ index: Int) {
        /*
        for i in 0...4 {
            if( index == i)
            {
                segmentedControl.setImage(UIImage(named:"Group 3191"), forSegmentAt: i)
            }
            else {
                segmentedControl.setImage(UIImage(named:"smile"), forSegmentAt: i)
            }
        }
         */
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
            changeIndexImage(index)
            vc = firstChildTabVC
            
        case TabIndex.secondChildTab.rawValue :
            changeIndexImage(index)
            vc = secondChildTabVC
            
        case TabIndex.thirdChildTab.rawValue :
            changeIndexImage(index)
            vc = thirdChildTabVC
            
        case TabIndex.fourChildTab.rawValue :
            changeIndexImage(index)
            vc = fourChildTabVC
            
        case TabIndex.fiveChildTab.rawValue :
            changeIndexImage(index)
            vc = fiveChildTabVC
            
        default:
            return nil
        }
        
        return vc
    }
}
