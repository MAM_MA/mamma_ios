//
//  IngreDetailVO.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2017. 1. 3..
//  Copyright © 2017년 김민수. All rights reserved.
//

import UIKit
import Kingfisher

class IngreDetailVO {
    var milk_image: String
    var milk_name: String
    var stage: Int
    var milk_grade:Int
    
    
    init(milk_image:String, milk_name:String, stage:Int, milk_grade:Int){
        self.milk_image = milk_image
        self.milk_name = milk_name
        self.stage = stage
        self.milk_grade = milk_grade
        
    }
    
    var imgType: UIImage {
        switch stage {
        case 0:
            return #imageLiteral(resourceName: "s")
        case 1:
            return #imageLiteral(resourceName: "one")
        case 2:
            return #imageLiteral(resourceName: "two")
        case 3:
            return #imageLiteral(resourceName: "three")
        case 4:
            return #imageLiteral(resourceName: "four")
        default:
            return #imageLiteral(resourceName: "special")
        }
    }
}
