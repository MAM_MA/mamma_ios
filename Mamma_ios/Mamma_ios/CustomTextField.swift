//
//  CustomTextField.swift
//  Mamma_ios
//
//  Created by 김민수 on 2017. 1. 4..
//  Copyright © 2017년 김민수. All rights reserved.
//
import UIKit

class CustomTextField: UITextField{
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    
}
