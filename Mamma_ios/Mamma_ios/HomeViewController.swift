//
//  HomeViewController.swift
//  Mamma_ios
//
//  Created by 김민수 on 2016. 12. 28..
//  Copyright © 2016년 김민수. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher



class HomeViewController: UIViewController {
    
    @IBAction func mrmilkAction(_ sender: Any) {
        if let svc = storyboard?.instantiateViewController(withIdentifier: "reviewDetail") as? ReviewDetailTableViewController {
            navigationController?.pushViewController(svc, animated: true)
        }
    }
    
    @IBAction func msmilkAction(_ sender: Any) {
        if let svc = storyboard?.instantiateViewController(withIdentifier: "searchDetail") as? SearchDetailTableViewController {
            navigationController?.pushViewController(svc, animated: true)
        }
        
        
    }
    let baseURL = "http://52.78.46.225:3000"
    
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var company1: UILabel!

    
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var company2: UILabel!
    
    @IBOutlet weak var imageView3: UIImageView!
    @IBOutlet weak var name3: UILabel!
    @IBOutlet weak var company3: UILabel!
    /*********************************************************************/
    
    
    @IBOutlet weak var r_image1: UIImageView!
    @IBOutlet weak var r_name1: UILabel!
    
    @IBOutlet weak var r_image2: UIImageView!
    @IBOutlet weak var r_name2: UILabel!
    
    @IBOutlet weak var r_image3: UIImageView!
    @IBOutlet weak var r_name3: UILabel!
    /*********************************************************************/

    
    @IBOutlet weak var i_label1: UILabel!
    @IBOutlet weak var i_label2: UILabel!
    
    
    
    @IBOutlet weak var tx : UITextField!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        
        //아무 곳을 누르면 hide
        tx.delegate = self
        
        
        
        //아무 곳을 누르면 hide
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(regMomViewController.DismissKeyboard))
        self.view.addGestureRecognizer(tap)

        
        
        
        
        getObjectFromServer1()
        getObjectFromServer2()
        getObjectFromServer3()
        
        
        
        
        
        name1.numberOfLines = 0;
        name2.numberOfLines = 0;
        name3.numberOfLines = 0;
        r_name1.numberOfLines = 0;
        r_name2.numberOfLines = 0;
        r_name3.numberOfLines = 0;
        i_label1.numberOfLines = 0;
        i_label2.numberOfLines = 0;
        
      scrollView.contentSize.height = 700//        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
    }
    
    

}
/*********************************************************************/



extension HomeViewController :  UITextFieldDelegate{
    
    //리턴 버튼을 누르면 hide
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when   'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        tx.resignFirstResponder()
       
        
        self.view.endEditing(true)
    }
    
    //리턴 버튼을 누르면 hide
    func DismissKeyboard(){
        self.view.endEditing(true)
    }
    //    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    //        self.view.endEditing(true)
    //    }
    
}





extension HomeViewController{
    
    func getObjectFromServer3() {
        
        Alamofire.request("\(baseURL)/homeview").responseJSON() { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
                    
                
                            if let ingre_name = data["result"]["dayingredient"]["ingre_name"].string {
                                self.i_label1.text = ingre_name
                            }
                            
                            if let overview =  data["result"]["dayingredient"]["overview"].string {
                                self.i_label2.text = overview
                            }

                        
                        }
                
                break
            case .failure(let err) :
                print(err)
            }
        }
    }
    
    
    
    
}











extension HomeViewController{
    
    

    
    func views2(index: Int) -> (UIImageView, UILabel) {
        
        switch index {
        case 1:
            return (r_image2, r_name2)
        case 2:
            return (r_image3, r_name3)
        default:
            return (r_image1, r_name1)
        }
    }
    
    func getObjectFromServer2() {
        
        Alamofire.request("\(baseURL)/homeview").responseJSON() { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
//                    print(data)
                    if let array =  data["result"]["reviewRank"].array{
                        for (i, item) in array.enumerated() {
                            let tuple = self.views2(index: i)
                            
                            if let milk_name = item["milk_name"].string {
                                tuple.1.text = milk_name
                            }
                        
                            if let strUrl = item["milk_image"].string {
                                
                                if let url = URL(string: strUrl) {
                                    tuple.0.kf.setImage(with: url)
                                }
                            }
                            
                        }
                    }
                    
                }
                
                break
            case .failure(let err) :
                print(err)
            }
        }
    }

}
extension HomeViewController{
    func views(index: Int) -> (UIImageView, UILabel, UILabel) {
        
        switch index {
        case 1:
            return (imageView2, name2, company2)
        case 2:
            return (imageView3, name3, company3)
        default:
            return (imageView1, name1, company1)
        }
    }
    
    func getObjectFromServer1() {
        
        Alamofire.request("\(baseURL)/homeview").responseJSON() { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
//                    print(data)
                    if let array =  data["result"]["searchrank"].array{
                        for (i, item) in array.enumerated() {
                            let tuple = self.views(index: i)
                            if let milk_name = item["milk_name"].string {
                                tuple.1.text = milk_name
                            }
                            if let milk_company = item["milk_company"].string {
                                tuple.2.text = milk_company
                            }
                            if let strUrl = item["milk_image"].string {
                                
                                if let url = URL(string: strUrl) {
                                    tuple.0.kf.setImage(with: url)
                                }
                            }
                            
                        }
                    }
                    
                }
                
                break
            case .failure(let err) :
                print(err)
            }
        }
    }

}
