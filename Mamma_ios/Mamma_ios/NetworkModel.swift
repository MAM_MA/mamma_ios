//
//  NetworkModel.swift
//  Mamma_ios
//
//  Created by 김민수 on 2017. 1. 2..
//  Copyright © 2017년 김민수. All rights reserved.
//

class NetworkModel {
    
    //aws EC2 인스턴스의 주소
    //internal let baseURL = "http://188.166.181.182:5555"
    internal let baseURL = "http://52.78.46.225:3000"
    
    var view: NetworkCallback
    
    init(_ view: NetworkCallback) {
        self.view = view
    }
    
    //string
    func gsno(_ value: String?) -> String {
        if let value_ = value {
            return value_
        } else {
            return ""
        }
    }
    
    //int
    func gino(_ value: Int?) -> Int {
        if let value_ = value {
            return value_
        } else {
            return 0
        }
    }
}
