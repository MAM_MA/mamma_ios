//
//  ExtensionControl.swift
//  Mamma_ios
//
//  Created by 김민수 on 2017. 1. 2..
//  Copyright © 2017년 김민수. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    public func imageFromUrl(_ urlString: String?, defaultImgPath : String) {
        let defaultImg = UIImage(named: defaultImgPath)
        if let url = urlString {
            if url.isEmpty {
                self.image = defaultImg
            } else {
                self.kf.setImage(with: URL(string: url), placeholder: defaultImg, options: [.transition(ImageTransition.fade(0.5))])
            }
        } else {
            self.image = defaultImg
        }
    }
}

extension UIViewController {
    func gsno(_ value: String?) -> String {
        if let value_ = value {
            return value_
        } else {
            return ""
        }
    }
    
    func gino(_ value: Int?) -> Int {
        if let value_ = value {
            return value_
        } else {
            return 0
        }
    }
    
    func simpleAlert(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "확인", style: .default)
        alert.addAction(okAction)
        present(alert, animated: true)
    }
    
    func networkFailed() {
        simpleAlert(title: "네트워크 오류", msg: "인터넷 연결을 확인해주세요.")
    }
}

extension UINavigationController {
    
    var previousViewController: UIViewController? {
        let length = self.viewControllers.count
        let previousViewController: UIViewController? = length >= 2 ? self.viewControllers[length-2] : nil
        
        return previousViewController
    }
    
}

extension UIView {
    
    func shake(max: Double) {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.4
        animation.values = [-max, max, -max*(2/3), max*(2/3), -max*(1/3), max*(1/3), 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    func expand() {
        self.transform = self.transform.scaledBy(x: 0.25, y: 0.25)
        
        UIView.animate(withDuration: 1.2,
                       delay: 0.0,
                       usingSpringWithDamping: 1.5,
                       initialSpringVelocity: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: ({
                        self.transform = self.transform.scaledBy(x: 4.0, y: 4.0)
                       }),
                       completion: nil)
    }
}
