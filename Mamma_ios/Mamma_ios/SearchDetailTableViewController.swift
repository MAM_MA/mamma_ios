import UIKit
import Alamofire
import Kingfisher

class SearchDetailTableViewController: UITableViewController, NetworkCallback{
    
    var searchList = [SearchDetailVO]()
    var milk_image: String?
    var milk_name: String?
    var stage: Int?
    var milk_grade:Int?
    
    override func viewDidAppear(_ animated: Bool) {
        print("SearchDetail Appear")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        
        let _image: UIImage = UIImage(named: "3_signup_back.png")!
        var backBtn = UIBarButtonItem()
        backBtn = UIBarButtonItem(image: _image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(SearchDetailTableViewController.goBack))
        self.navigationItem.leftBarButtonItem = backBtn
        
        
        print("SearchDetail Load")
        let model = SearchDetailModel(self)
        model.getSearchDetail(milk_image: gsno(milk_image), milk_name: gsno(milk_name), stage: gino(stage), milk_grade: gino(milk_grade),url : "msmilk")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func networkResult(resultData: Any, code: Int) {
        searchList = resultData as! [SearchDetailVO]
        tableView.reloadData()
    }
    
    func goBack() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}


extension SearchDetailTableViewController{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchDetailTableViewCell
        
        let item = searchList[indexPath.row]
        
        
        if let url = URL(string: item.milk_image){
            cell.milk_imageView.kf.setImage(with: url)
            cell.milk_imageView.contentMode = .scaleAspectFit
        }
        cell.txtNumber.text = "\(indexPath.row + 1)"
        cell.milk_nametxt.text = gsno(item.milk_name)
        cell.milk_nametxt.lineBreakMode = NSLineBreakMode.byWordWrapping
        //        cell.milk_nametxt = "Line1\nLine2"
        cell.milk_nametxt.numberOfLines = 2
        cell.imgType.image = item.imgType
        
        return cell
    }
}
