//
//  IngredientsRankViewController.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2017. 1. 2..
//  Copyright © 2017년 김민수. All rights reserved.
//

import UIKit

class IngredientsRankViewController: UIViewController {
    
    @IBOutlet var segmentedControl: UISegmentedControl!
    
    @IBAction func IngredientsRank_Click(_ sender: Any) {
        if(segmentedControl.selectedSegmentIndex == 0)
        {
            print("select 1 ")
        }
        else if(segmentedControl.selectedSegmentIndex == 1)
        {
            print("select 2 ")
        }
        else if(segmentedControl.selectedSegmentIndex == 2)
        {
            print("select 3 ")
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
