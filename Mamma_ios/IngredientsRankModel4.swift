//
//  IngredientsRank.swift
//  Mamma_ios
//
//  Created by 기용 신 on 2017. 1. 3..
//  Copyright © 2017년 김민수. All rights reserved.
//

import Alamofire
import SwiftyJSON

class IngredientsRankModel4: NetworkModel{
    
    func getReviewDetail(milk_image:String, milk_name:String, stage:Int, milk_grade:Int ){
        let BASEURL = "\(baseURL)/ingre_ranking/"
        var query = "/4"
        
        var tmpPara : String = ""
        tmpPara = UserDefaults.standard.string(forKey: "stepProduct")!
        
        
        
        query = tmpPara + "/3" // Ok
        
        let encodeQuery = query.addingPercentEscapes(using: .utf8)
        let requestUrl = BASEURL + encodeQuery!
        
        
        
        
        Alamofire.request(requestUrl).responseJSON() { res in
            //  Alamofire.request("\(baseURL)/ingre_ranking/비타민B1/1").responseJSON { res in
            switch res.result {
            case .success :
                if let value = res.result.value {
                    let data = JSON(value)
                    var tempList = [IngreDetailVO]()
                    if let array = data["result"]["ingre_ranking"].array{
                        for item in array{
                            
                            let uvo = IngreDetailVO(milk_image: self.gsno(item["milk_image"].string),
                                                    milk_name: self.gsno(item["milk_name"].string),
                                                    stage: self.gino(item["stage"].int),
                                                    milk_grade: self.gino(item["milk_grade"].int)
                            )
                            tempList.append(uvo)
                        }
                        self.view.networkResult(resultData: tempList, code: 0)
                    }
                }
                break
            case .failure(let err) :
                print(err)
                self.view.networkFailed()
            }
        }
    }
}
